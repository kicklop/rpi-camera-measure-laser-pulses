from __future__ import (
    unicode_literals,
    absolute_import,
    print_function,
    division,
    )

import io
import time
from gpiozero import Button
from picamera import PiCamera
import numpy as np
import png
import matplotlib.pyplot as plt
import PIL

shot = "56766_T-192ms"#"56727_trigtest_T-192ms"
#trigger 192 ms pred vystrelem by melo byt nejlepsi (z mereni s testovaci kamerou)
#usekava 20 ms z obrazku na obe strany a nejcasteji ma dobry obrazek
to_be_saved = True
to_rgb = False

#values to set for camera
my_shutter_speed = 66570 #jeste nastavit, v us, nejmensi hodnoty:9+x*19
#(18.904 us per line (proč je minimum 9 us?), 18.904*2464 = 46.58 ms per image
my_framerate = 30

my_sensor_mode = 2 #3280x2464, Bayer jine neumi, preview je videt, kdyz je sensor mode 0
my_resolution = (3280,2464)
my_awb_mode = 'off'
my_awb_gains = (1,1)
my_drc_strength = 'off' #other than 'off' will overwrite awb_gains, awb_mode
my_brightness = 50 #nema vliv na Bayer mereni
my_contrast = 0 #nema vliv na Bayer mereni
my_exposure_compensation = 0 #default
my_image_denoise = False #nejspis nema vliv na Bayer, a kdyby melo, tak chci nastaveni False
my_image_effect = 'none' #default
my_sharpness = 0 #nema vliv na Bayer

trigger = Button("BOARD40", pull_up=False, bounce_time=None) #tady to nesmi byt,
#pokud pouzivam trigger v _start_capture v camera.py
stream = io.BytesIO()


with PiCamera(resolution=my_resolution, sensor_mode=my_sensor_mode,
            framerate=my_framerate) as camera:
    # Let the camera warm up for a couple of seconds
    time.sleep(1)

    print(camera.sensor_mode)
    print(camera.framerate_range)
    print(camera.resolution)
    camera.awb_mode = my_awb_mode
    camera.awb_gains = my_awb_gains
    camera.drc_strength = my_drc_strength
    camera.brightness = my_brightness
    camera.contrast = my_contrast
    camera.exposure_compensation = my_exposure_compensation
    camera.shutter_speed = my_shutter_speed
    camera.image_denoise = my_image_denoise
    camera.image_effect = my_image_effect
    camera.sharpness = my_sharpness
    print(camera.exposure_speed)

	
    time.sleep(1)
	

	#preview nefunguje pro sensor_mode = 2 (proc jsem to psal, kdyz to funguje?)
    camera.start_preview()
    camera.preview.fullscreen = False
    camera.preview.window = (0,0,640,480) #(x, y, width, height)

    print("Kamera snímá, nastav gain. (Sviť před vypsáním zprávy pro jednodušší nastavení.)")
    gain_is_set = False
    while not gain_is_set:
        if camera.analog_gain == 1 and camera.digital_gain == 1: #minimalni hodnota analog_gain i digital_gain je 1
            gain_is_set = True
            exposure_mode = 'off'

    assert camera.analog_gain == 1
    assert camera.digital_gain == 1
    print("Digital a analog gain jsou na 1.")
    print(f"Exposure speed je {camera.exposure_speed} us.")
    print(f"Cislo shotu je {shot}.")

    camera.stop_preview()
	
    # After trigger capture the image, including the Bayer data
    # trigger v camera.py _start_capture() dost zpomalil vytvoreni snimku
    #(cca 1s namisto cca 400ms)
    print(camera.sensor_mode)
    print(camera.framerate_range)
    print(camera.resolution)
    print(camera.exposure_speed)
    print("Waiting for trigger")
    #input("Press Enter to capture...")
    trigger.wait_for_press(timeout=None)
    camera.capture(stream, format='jpeg', bayer=True)
    print(camera.sensor_mode)
    print(camera.framerate_range)
    print(camera.resolution)
    print(camera.exposure_speed)
    print("Fotka dokoncena.")

# Extract the raw Bayer data from the end of the stream, check the
# header and strip if off before converting the data into a numpy array

offset = 10270208
data = stream.getvalue()[-offset:]
assert data[:4] == b'BRCM' #pak zakomentovat, abych si nesmazal obrazek
#a misto toho napsat jen varovani pres if
data = data[32768:]
data = np.frombuffer(data, dtype=np.uint8)

#save jpeg file (doesnt have exif data)
#stream.seek(0)
#byte_img = PIL.Image.open(stream)
#byte_img.save("trig_test/"+shot+".jpg", "JPEG")


# For the V1 module, the data consists of 1952 rows of 3264 bytes of data.
# The last 8 rows of data are unused (they only exist because the maximum
# resolution of 1944 rows is rounded up to the nearest 16).
#
# For the V2 module, the data consists of 2480 rows of 4128 bytes of data.
# There's actually 2464 rows of data, but the sensor's raw size is 2466
# rows, rounded up to the nearest multiple of 16: 2480.
#
# Likewise, the last few bytes of each row are unused (why?). Here we
# reshape the data and strip off the unused bytes.

reshape, crop = ((2480, 4128), (2464, 4100))
data = data.reshape(reshape)[:crop[0], :crop[1]]

# Horizontally, each row consists of 10-bit values. Every four bytes are
# the high 8-bits of four values, and the 5th byte contains the packed low
# 2-bits of the preceding four values. In other words, the bits of the
# values A, B, C, D and arranged like so:
#
#  byte 1   byte 2   byte 3   byte 4   byte 5
# AAAAAAAA BBBBBBBB CCCCCCCC DDDDDDDD AABBCCDD
#
# Here, we convert our data into a 16-bit array, shift all values left by
# 2-bits and unpack the low-order bits from every 5th byte in each row,
# then remove the columns containing the packed bits

data = data.astype(np.uint16) << 2
for byte in range(4):
    data[:, byte::5] |= ((data[:, 4::5] >> ((4 - byte) * 2)) & 0b11)
data = np.delete(data, np.s_[4::5], 1)

# Now to split the data up into its red, green, and blue components. The
# Bayer pattern of the OV5647 sensor is BGGR. In other words the first
# row contains alternating green/blue elements, the second row contains
# alternating red/green elements, and so on as illustrated below:
#
# GBGBGBGBGBGBGB
# RGRGRGRGRGRGRG
# GBGBGBGBGBGBGB
# RGRGRGRGRGRGRG
#
# Please note that if you use vflip or hflip to change the orientation
# of the capture, you must flip the Bayer pattern accordingly

if to_rgb:
    rgb = np.zeros(data.shape + (3,), dtype=data.dtype)
    rgb[1::2, 0::2, 0] = data[1::2, 0::2] # Red
    rgb[0::2, 0::2, 1] = data[0::2, 0::2] # Green
    rgb[1::2, 1::2, 1] = data[1::2, 1::2] # Green
    rgb[0::2, 1::2, 2] = data[0::2, 1::2] # Blue

if to_be_saved:
    #uloz jako png (i z png se to da nahrat zpet do pythonu)
    with open(shot+"_true.png", "wb") as f:
        writer = png.Writer(width=data.shape[1], height=data.shape[0], bitdepth=16, greyscale=True)
        data2list = data.tolist()
        writer.write(f, data2list)
    plt.imsave(shot+"_scaled.png", data)